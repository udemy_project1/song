import React from "react";
import playIcon from "../../assets/play-icon.svg";
import pauseIcon from "../../assets/pause-icon.svg";
import { useDispatch, useSelector } from "react-redux";
import { toggleLecture } from "../../features/playlist";

export default function TogglePlayButton() {
	const playlistValues = useSelector((state) => state.playlist);
	const dispatch = useDispatch();
	return (
		<button
			onClick={() => dispatch(toggleLecture())}
			className="w-14 h-14 shadow-md bg-slate-50 rounded-full flex justify-center items-center outline-none">
			<img
				src={playlistValues?.play ? pauseIcon : playIcon}
				className="w-20 h-20"
				alt="Toggle button"
			/>
		</button>
	);
}
