import React from "react";
import nextIcon from "../../assets/next-icon.svg";
import { useDispatch, useSelector } from "react-redux";
import { nextSong } from "../../features/playlist";

export default function NextButton() {
	const playlistValues = useSelector((state) => state.playlist);

	const dispatch = useDispatch();
	return (
		<button
			onClick={() => dispatch(nextSong(playlistValues?.songs?.findIndex((obj) => obj.id === playlistValues?.currentMusicID) + 1))}
			className="w-9 h-9 ml-4 bg-slate-400 rounded-full flex justify-center items-center">
			<img
				src={nextIcon}
				className="w-5 h-5"
				alt="NextIcon"
			/>
		</button>
	);
}
