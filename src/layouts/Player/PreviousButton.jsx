import React from "react";
import previousIcon from "../../assets/prev-icon.svg";
import { useDispatch, useSelector } from "react-redux";
import { previousSong } from "../../features/playlist";

export default function PreviousButton() {
	const playlistValues = useSelector((state) => state.playlist);

	const dispatch = useDispatch();
	return (
		<button
			onClick={() => dispatch(previousSong(playlistValues?.songs?.findIndex((obj) => obj.id === playlistValues?.currentMusicID) - 1))}
			className="w-9 h-9 mr-4 bg-slate-400 rounded-full flex justify-center items-center">
			<img
				src={previousIcon}
				className="w-5 h-5"
				alt="PrevIcon"
			/>
		</button>
	);
}
