import React from "react";
import { useSelector } from "react-redux";
import { formatValue } from "../../utils/formatValue";

export default function Progress() {
	const progressValues = useSelector((state) => state.progress);
	const handleProgressClick = (e) => {
		const player = document.getElementById("audio-player");
		const rect = e.target.getBoundingClientRect();
		const width = rect.width;
		const x = e.clientX - rect.left;

		player.currentTime = (x / width) * progressValues.totalDuration;
	};
	return (
		<div className="max-w-[800px] mx-auto">
			<div
				onClick={handleProgressClick}
				className="bg-slate-900 h-2 rounded cursor-pointer overflow-hidden">
				<div
					style={{ transform: `scaleX(${progressValues.current / progressValues.totalDuration})` }}
					className="bg-indigo-400  origin-left h-full pointer-events-none"></div>
			</div>
			<div className="flex justify-between items-center">
				<span>{formatValue(progressValues?.current)}</span>
				<span>{formatValue(progressValues?.totalDuration)}</span>
			</div>
		</div>
	);
}
