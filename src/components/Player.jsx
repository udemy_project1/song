import React from "react";
import { useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fillDurationVariables, updateProgress } from "../features/progress";

export default function Player() {
	const dispatch = useDispatch();
	const playlistValues = useSelector((state) => state.playlist);

	const audioRef = useRef();

	const handleLoadedData = (e) => {
		console.log("icicicici handleLodedData");
		if (playlistValues.songs) {
			dispatch(fillDurationVariables({ currentTime: e.target.currentTime, totalDuration: e.target.duration }));
		}
	};
	const handleTimeUpdate = (e) => {
		console.log("icicicici handleTimeUpdate");
		dispatch(updateProgress(e.target.currentTime));
	};
	useEffect(() => {
		if (playlistValues.songs && playlistValues.play) {
			audioRef.current.play();
		} else {
			audioRef.current.pause();
		}
	}, [playlistValues]);
	return (
		<audio
			id="audio-player"
			src={playlistValues?.songs?.find((obj) => obj.id === playlistValues?.currentMusicID).url}
			onLoadedData={handleLoadedData}
			onTimeUpdate={handleTimeUpdate}
			ref={audioRef}></audio>
	);
}
